import { IconButton, useColorMode } from '@chakra-ui/react'
import { BiSolidSun, BiSolidMoon } from 'react-icons/bi'

export const ToggleTheme = () => {
  const { toggleColorMode, colorMode } = useColorMode()
  return (
    <IconButton
      onClick={toggleColorMode}
      aria-label={'Toggle theme'}
      icon={colorMode === 'light' ? <BiSolidMoon size={20} /> : <BiSolidSun size={20} />}
    />
  )
}
