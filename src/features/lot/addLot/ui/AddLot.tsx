import { Button, HStack, Input } from '@chakra-ui/react'
import { FormEvent, useState } from 'react'
import { useAppDispatch } from '@/shared/model'
import { addLot } from '@/entities/lot'
import { colors } from '@/shared/constants/colors.ts'

export const AddLot = () => {
  const [titleLot, setTitleLot] = useState('')
  const dispatch = useAppDispatch()

  function add(e: FormEvent) {
    e.preventDefault()
    dispatch(
      addLot({
        id: Date.now(),
        title: titleLot,
        color: colors[Math.floor(Math.random() * colors.length)],
      }),
    )
    setTitleLot('')
  }

  return (
    <HStack as={'form'} spacing={'24px'} flex={'1'} onSubmit={(e) => add(e)}>
      <Input
        placeholder={'Название лота'}
        value={titleLot}
        onChange={(e) => setTitleLot(e.target.value)}
        size={'lg'}
      />
      <Button size={'lg'} type={'submit'}>
        Добавить
      </Button>
    </HStack>
  )
}
