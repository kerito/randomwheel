import { IconButton } from '@chakra-ui/react'
import { useAppDispatch } from '@/shared/model'
import { removeLots } from '@/entities/lot'
import { FaTrashArrowUp } from 'react-icons/fa6'

export const RemoveLots = () => {
  const dispatch = useAppDispatch()
  return (
    <IconButton
      onClick={() => dispatch(removeLots())}
      aria-label={'Remove lots'}
      title={'Удалить все лоты'}
      icon={<FaTrashArrowUp />}
      size={'lg'}
    />
  )
}
