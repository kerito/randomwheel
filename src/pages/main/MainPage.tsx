import { LotsList, Wheel } from '@/widget'
import { Grid } from '@chakra-ui/react'

const MainPage = () => {
  return (
    <Grid templateColumns={'70% 30%'} pt={'30px'} justifyItems={'center'}>
      <Wheel />
      <LotsList />
    </Grid>
  )
}

export default MainPage
