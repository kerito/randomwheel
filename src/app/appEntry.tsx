import React from 'react'
import ReactDOM from 'react-dom/client'
import { Provider as ReduxProvider } from 'react-redux'
import { appStore, persistedStore } from '@/app/appStore.ts'
import { ChakraProvider } from '@chakra-ui/react'
import { BaseLayout } from '@/app/layouts/BaseLayout.tsx'
import { PersistGate } from 'redux-persist/integration/react'
import '@/shared/styles/index.css'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <ReduxProvider store={appStore}>
      <PersistGate loading={null} persistor={persistedStore}>
        <ChakraProvider>
          <BaseLayout />
        </ChakraProvider>
      </PersistGate>
    </ReduxProvider>
  </React.StrictMode>,
)
