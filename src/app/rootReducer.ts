import { combineReducers } from '@reduxjs/toolkit'
import { lotSlice } from '@/entities/lot'

export const rootReducer = combineReducers({
  [lotSlice.name]: lotSlice.reducer,
})
