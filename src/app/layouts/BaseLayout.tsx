import { Layout } from '@/shared/ui'
import { Header } from '@/widget'
import MainPage from '@/pages/main/MainPage.tsx'

export const BaseLayout = () => {
  return <Layout headerSlot={<Header />} mainSlot={<MainPage />} />
}
