export { LotRow } from './ui/LotRow.tsx'
export { lotSlice, selectLot, addLot, removeLot, removeLots, renameLot } from './model/slice.ts'
export { type ILot } from './model/types.ts'
