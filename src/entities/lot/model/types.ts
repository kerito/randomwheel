export interface ILot {
  id: number
  title: string
  color?: string
}
