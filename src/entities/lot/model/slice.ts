import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { ILot } from './types.ts'
import { RootState } from '@/app/appStore.ts'

const initialState: ILot[] = []

export const lotSlice = createSlice({
  name: 'lot',
  initialState,
  reducers: {
    addLot: (state, action: PayloadAction<ILot>) => {
      return [...state, action.payload]
    },
    removeLot: (state, action: PayloadAction<number>) => {
      return state.filter((lot: ILot) => lot.id !== action.payload)
    },
    removeLots: () => {
      return []
    },
    renameLot: (state, action: PayloadAction<ILot>) => {
      return state.map((lot: ILot) =>
        lot.id === action.payload.id ? { ...lot, title: action.payload.title } : lot,
      )
    },
  },
})

export const selectLot = (state: RootState) => state.lot
export const { addLot, removeLot, removeLots, renameLot } = lotSlice.actions
