import React, { FC, forwardRef } from 'react'
import { ILot } from '../model/types.ts'
import {
  Badge,
  IconButton,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
} from '@chakra-ui/react'
import { FaTrash } from 'react-icons/fa'
import { useAppDispatch } from '@/shared/model'
import { removeLot, renameLot } from '@/entities/lot'
import { motion } from 'framer-motion'

interface ILotRowProp {
  lot: ILot
  number: number
}

export const LotRow: FC<ILotRowProp> = forwardRef(
  ({ lot, number }, ref: React.ForwardedRef<HTMLDivElement>) => {
    const dispatch = useAppDispatch()

    function rename(e: React.FocusEvent<HTMLInputElement, Element>, title: string) {
      if (e.target.value === title) {
        return
      }
      dispatch(renameLot({ id: lot.id, title: e.target.value }))
    }

    return (
      <InputGroup
        as={motion.div}
        layout
        initial={{ scale: 0.8, opacity: 0 }}
        animate={{ scale: 1, opacity: 1 }}
        exit={{ scale: 0.8, opacity: 0 }}
        transition={{ type: 'spring' }}
        key={lot.id}
        ref={ref}
        size={'lg'}
        alignItems={'center'}
      >
        <InputLeftElement>
          <Badge fontSize={'md'}>#{number}</Badge>
        </InputLeftElement>
        <Input
          placeholder={'Название'}
          defaultValue={lot.title}
          onBlur={(e) => rename(e, lot.title)}
        />
        <InputRightElement>
          <IconButton
            onClick={() => dispatch(removeLot(lot.id))}
            aria-label={'Trash icon'}
            icon={<FaTrash />}
            title={'Удалить слот'}
          />
        </InputRightElement>
      </InputGroup>
    )
  },
)
