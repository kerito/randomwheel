import { AddLot } from '@/features'
import { LotRow, selectLot } from '@/entities/lot'
import { useAppSelector } from '@/shared/model'
import { RemoveLots } from '@/features/lot'
import { Flex, HStack } from '@chakra-ui/react'
import { AnimatePresence } from 'framer-motion'
import { SLotsList } from './styled.ts'

export const LotsList = () => {
  const lots = useAppSelector(selectLot)

  return (
    <SLotsList>
      <HStack spacing={'24px'}>
        <AddLot />
        <RemoveLots />
      </HStack>
      <Flex direction={'column'} gap={'14px'} mt={'24px'}>
        <AnimatePresence mode={'popLayout'}>
          {lots.map((lot, index) => (
            <LotRow key={lot.id} lot={lot} number={++index} />
          ))}
        </AnimatePresence>
      </Flex>
    </SLotsList>
  )
}
