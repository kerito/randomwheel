import styled from '@emotion/styled'

export const SLotsList = styled.div`
  max-height: calc(100vh - 130px);
  overflow-y: auto;
  overflow-x: hidden;
  padding: 5px 15px 5px 5px;
`
