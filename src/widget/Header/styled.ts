import styled from '@emotion/styled'

export const SHeader = styled.header({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
  padding: '15px 0',
})

export const STitle = styled.header({
  fontSize: '32px',
  fontWeight: 'bolder',
})

export const SIcons = styled.div({
  display: 'flex',
  alignItems: 'center',
  gap: '8px',
})
