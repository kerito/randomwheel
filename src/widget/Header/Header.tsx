import { SiGitlab } from 'react-icons/si'
import { IconButton } from '@chakra-ui/react'
import { SHeader, SIcons, STitle } from './styled.ts'
import { ToggleTheme } from '@/features'

export const Header = () => {
  return (
    <SHeader>
      <STitle>RandomWheel</STitle>
      <SIcons>
        <a href={'https://gitlab.com/'} target={'_blank'}>
          <IconButton aria-label={'Gitlab icon'} icon={<SiGitlab />} />
        </a>
        <ToggleTheme />
      </SIcons>
    </SHeader>
  )
}
