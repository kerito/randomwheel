import styled from '@emotion/styled'
import { motion } from 'framer-motion'

export const SDealWheel = styled.div`
  --size: clamp(250px, 80vmin, 700px);
  --lg-hs: 0 3%;
  --lg-stop: 50%;
  --lg: linear-gradient(
    hsl(var(--lg-hs) 0%) 0 var(--lg-stop),
    hsl(var(--lg-hs) 20%) var(--lg-stop) 100%
  );
  position: relative;
  display: grid;
  grid-gap: calc(var(--size) / 20);
  align-items: start;
  font-size: calc(var(--size) / 32);
  width: min-content;
`

export const SSpinner = styled(motion.ul)`
  position: relative;
  display: grid;
  align-items: center;
  grid-template-areas: 'spinner';
  width: var(--size);
  height: var(--size);
  transform: rotate(calc(var(--rotate, 25) * 1deg));
  border-radius: 50%;

  * {
    grid-area: spinner;
  }
`

export const SSpinnerLot = styled(motion.li)`
  display: flex;
  align-items: center;
  padding: 0 calc(var(--size) / 6) 0 calc(var(--size) / 50);
  width: 50%;
  height: 50%;
  transform-origin: center right;
  user-select: none;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`
