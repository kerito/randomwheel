import { SDealWheel, SSpinner, SSpinnerLot } from './styled.ts'
import { useAppSelector } from '@/shared/model'
import { selectLot } from '@/entities/lot'
import { AnimatePresence, useMotionValue, useMotionValueEvent } from 'framer-motion'
import { Button } from '@chakra-ui/react'
import { useState } from 'react'

export const Wheel = () => {
  const lots = useAppSelector(selectLot)
  const [start, setStart] = useState(false)

  function createConicGradient() {
    if (!lots.length) {
      return {
        background: `conic-gradient(from -90deg, var(--chakra-colors-blue-400) 0 100%`,
      }
    }

    return {
      background: `conic-gradient(from -90deg, ${lots
        .map(
          ({ color }, i) =>
            `var(--chakra-colors-${color}-400) 0 ${(100 / lots.length) * (lots.length - i)}%`,
        )
        .reverse()})`,
    }
  }

  const rotate = useMotionValue(0)

  useMotionValueEvent(rotate, 'animationComplete', () => {
    setStart(false)
  })

  return (
    <SDealWheel style={lots.length >= 60 ? { fontSize: 0 } : {}}>
      <SSpinner animate={start ? { rotate: 360 } : {}} style={{ ...createConicGradient(), rotate }}>
        <AnimatePresence>
          {lots.map(({ id, title }, i) => (
            <SSpinnerLot
              initial={{ scale: 0.8, opacity: 0 }}
              animate={{
                scale: 1,
                opacity: 1,
                rotate: `${(360 / lots.length) * i * -1 - Math.floor(180 / lots.length)}deg`,
              }}
              exit={{ scale: 0.8, opacity: 0 }}
              key={id}
            >
              {title}
            </SSpinnerLot>
          ))}
        </AnimatePresence>
      </SSpinner>
      <Button onClick={() => setStart(true)}>Начать</Button>
    </SDealWheel>
  )
}
