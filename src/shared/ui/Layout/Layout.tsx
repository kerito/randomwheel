import { Container } from '@chakra-ui/react'
import { FC, ReactNode } from 'react'

interface ILayoutProp {
  headerSlot?: ReactNode
  mainSlot?: ReactNode
}

export const Layout: FC<ILayoutProp> = ({ headerSlot, mainSlot }) => {
  return (
    <Container maxW={'full'}>
      {headerSlot}
      <main>{mainSlot}</main>
    </Container>
  )
}
